require 'spec_helper'

require 'gitlab/triage/policies/rule_policy'
require 'gitlab/triage/policies_resources/rule_resources'

describe Gitlab::Triage::Policies::RulePolicy do
  include_context 'with network context'

  let(:type) { 'issues' }
  let(:name) { 'Policy name' }

  let(:actions) { { summarize: { item: '' } } }
  let(:policy_spec) { { name: name, actions: actions } }
  let(:resources) { Gitlab::Triage::PoliciesResources::RuleResources.new([]) }

  subject { described_class.new(type, policy_spec, resources, network) }

  describe '#build_issue' do
    it 'delegates to EntityBuilders::IssueBuilder' do
      expect(Gitlab::Triage::EntityBuilders::IssueBuilder)
        .to receive(:new)
        .with(
          type: type,
          policy_spec: policy_spec,
          action: actions[:summarize],
          resources: resources,
          network: network)
        .and_call_original

      subject.build_issue
    end
  end
end
