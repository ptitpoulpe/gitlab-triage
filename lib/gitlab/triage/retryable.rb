module Gitlab
  module Triage
    module Retryable
      MAX_RETRIES = 3

      attr_accessor :tries

      def execute_with_retry(exception_types = [StandardError])
        @tries = 0

        until maximum_retries_reached?
          begin
            @tries += 1
            result = yield
            break
          rescue *exception_types
            raise if maximum_retries_reached?
          end
        end

        result
      end

      private

      def maximum_retries_reached?
        tries == MAX_RETRIES
      end
    end
  end
end
