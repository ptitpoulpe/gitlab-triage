require 'spec_helper'

require 'gitlab/triage/resource/label'

describe Gitlab::Triage::Resource::Label do
  include_context 'with network context'

  let(:source_id_name) { :project_id }
  let(:resource) do
    {
      id: 1,
      source_id_name => 123,
      name: 'bug',
      description: 'a bug',
      color: '#d9534f',
      priority: 10,
      added_at: added_at.iso8601
    }
  end

  let(:added_at) { Time.new(2017, 1, 1) }

  subject { described_class.new(resource, network: network) }

  it_behaves_like 'resource fields'

  describe '#exist?' do
    context 'when source is project' do
      context 'when label exists' do
        it 'returns true' do
          expect(network).to receive(:query_api).with("#{base_url}/projects/123/labels/bug?per_page=100").and_return([{ name: 'bug' }])

          expect(subject.exist?).to eq(true)
        end
      end

      context 'when label does not exist' do
        it 'returns false' do
          expect(network).to receive(:query_api).with("#{base_url}/projects/123/labels/bug?per_page=100").and_return([])

          expect(subject.exist?).to eq(false)
        end
      end

      context 'when label name contains a space' do
        before do
          resource[:name] = 'a bug'
        end

        it 'encodes the space as %20' do
          expect(network).to receive(:query_api).with("#{base_url}/projects/123/labels/a%20bug?per_page=100").and_return([{ name: 'a bug' }])

          expect(subject.exist?).to eq(true)
        end
      end
    end

    context 'when source is group' do
      let(:source_id_name) { :group_id }

      context 'when label exists' do
        it 'returns true' do
          expect(network).to receive(:query_api_cached).with("#{base_url}/groups/123/labels/bug?per_page=100").and_return([{ name: 'bug' }])

          expect(subject.exist?).to eq(true)
        end
      end

      context 'when label does not exist' do
        it 'returns false' do
          expect(network).to receive(:query_api_cached).with("#{base_url}/groups/123/labels/bug?per_page=100").and_return([])

          expect(subject.exist?).to eq(false)
        end
      end
    end
  end
end
