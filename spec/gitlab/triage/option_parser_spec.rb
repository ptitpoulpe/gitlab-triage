# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Triage::OptionParser do
  subject(:options) { described_class.parse(argv) }

  describe '--policies-file' do
    context 'when passed single file' do
      let(:argv) { %w[-f file1.yml] }

      it { expect(options.policies_files).to eq(Set.new(['file1.yml'])) }
    end

    context 'when file passed multiply times' do
      let(:argv) { %w[-f file1.yml -f file2.yml] }

      it { expect(options.policies_files).to eq(Set.new(['file1.yml', 'file2.yml'])) }
    end

    context 'when files is not unique' do
      let(:argv) { %w[-f file1.yml -f file1.yml] }

      it { expect(options.policies_files).to eq(Set.new(['file1.yml'])) }
    end
  end

  describe '--all-projects' do
    context 'when set' do
      let(:argv) { ['--all-projects'] }

      it { expect(options.all).to be true }
    end

    context 'when not set' do
      let(:argv) { [] }

      it { expect(options.all).to be false }
    end
  end
end
